(function ($) {

    //DOM ready body class
    var DOMready = (function () {
        $(function () {
            $('.counter').counterUp({
                delay: 10,
                time: 1000
            });
        });
    }());

    /* ================================================================================================== */
    /* Smooth Scrolling Module */
    /* ================================================================================================== */

    // http://css-tricks.com/snippets/jquery/smooth-scrolling/
    var smoothScrollingModule = (function () {
        console.log('S');
        $(function () {
            $('#menu nav a').click(function () {
                console.log('P');
                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                        $('html,body').animate({
                            scrollTop: target.offset().top - 110
                        }, 100);
                        return false;
                    }
                }
            });
        });
    }());

    /* ================================================================================================== */
    /* Nav Sticky Design */
    /* ================================================================================================== */
    var navDesign = (function () {
        $(function () {
            var yourNavigation = $("#navigation-bar");
            stickyDiv = "sticky";
            yourHeader = $('#hero').height();

            stickyHeight = yourHeader - 65;

            $(window).scroll(function () {

                if ($(this).scrollTop() > stickyHeight) {
                    yourNavigation.addClass(stickyDiv);
                } else {
                    yourNavigation.removeClass(stickyDiv);
                }
            });
        });
    }());

    /* ================================================================================================== */
    /* Accordion Module */
    /* ================================================================================================== */

    var accordions = (function(){
    $(function(){
        $('.accordion-section').on('click', '.column .ipBlock > .ipWidget:first-child', function(e){
            e.preventDefault();
            var $this = $(this),
                accordion = $this.parents('.accordion-section'),
                items = accordion.find('.column'),
                item = $this.parents('.column');
            
            // If already open then close accordion
            if(item.hasClass('selected')){
                item
                    .removeClass('selected')
                    .find('.ipBlock >.ipWidget:first-child ~ .ipWidget')
                    .slideUp(300, function(){
                        item.removeClass('active');
                    });
            // Open accordion
            } else {
                items
                    .removeClass('selected')
                    .find('.ipBlock >.ipWidget:first-child ~ .ipWidget')
                    .slideUp(300, function(){
                        items.removeClass('active');
                    });

                item
                    .addClass('selected')
                    .find('.ipBlock >.ipWidget:first-child ~ .ipWidget')
                    .slideDown(300, function(){
                        item.addClass('active');
                        //console.log(item);
                        //$('html, body').animate({scrollTop: $(item).offset().top -150 }, 'slow');
                    });
            }  
        });
    });
    }());

    /* ================================================================================================== */
    /* Countdown Timer */
    /* ================================================================================================== */
    $(function () {

        var targetDate = new Date(Date.UTC(2022, 2, 29, 7));
        //var targetDate = new Date(Date.UTC(2022, 2, 30, 18, 0, 0));
        console.log(targetDate);
        var now = new Date();
      console.log(now);

        window.days = daysBetween(now, targetDate);
        var secondsLeft = secondsDifference(now, targetDate);
        window.hours = Math.floor(secondsLeft / 60 / 60);
        secondsLeft = secondsLeft - (window.hours * 60 * 60);
        window.minutes = Math.floor(secondsLeft / 60);
        secondsLeft = secondsLeft - (window.minutes * 60);
        console.log(secondsLeft);
        window.seconds = Math.floor(secondsLeft);

        startCountdown();
    });
    var interval;

    function daysBetween(date1, date2) {
        //Get 1 day in milliseconds
        var one_day = 1000 * 60 * 60 * 24;

        // Convert both dates to milliseconds
        var date1_ms = date1.getTime();
        var date2_ms = date2.getTime();

        // Calculate the difference in milliseconds
        var difference_ms = date2_ms - date1_ms;

        // Convert back to days and return
        return Math.round(difference_ms / one_day);
    }

    function secondsDifference(date1, date2) {
        //Get 1 day in milliseconds
        var one_day = 1000 * 60 * 60 * 24;

        // Convert both dates to milliseconds
        var date1_ms = date1.getTime();
        var date2_ms = date2.getTime();
        var difference_ms = date2_ms - date1_ms;
        var difference = difference_ms / one_day;
        var offset = difference - Math.floor(difference);
        return offset * (60 * 60 * 24);
    }



    function startCountdown() {
        $('#input-container').hide();
        $('#countdown-container').show();

        displayValue('#js-days', window.days);
        displayValue('#js-hours', window.hours);
        displayValue('#js-minutes', window.minutes);
        displayValue('#js-seconds', window.seconds);

        interval = setInterval(function () {
            if (window.seconds > 0) {
                window.seconds--;
                displayValue('#js-seconds', window.seconds);
            } else {
                // Seconds is zero - check the minutes
                if (window.minutes > 0) {
                    window.minutes--;
                    window.seconds = 59;
                    updateValues('minutes');
                } else {
                    // Minutes is zero, check the hours
                    if (window.hours > 0) {
                        window.hours--;
                        window.minutes = 59;
                        window.seconds = 59;
                        updateValues('hours');
                    } else {
                        // Hours is zero
                        window.days--;
                        window.hours = 23;
                        window.minutes = 59;
                        window.seconds = 59;
                        updateValues('days');
                    }
                    // $('#js-countdown').addClass('remove');
                    // $('#js-next-container').addClass('bigger');
                }
            }
        }, 1000);
    }


    function updateValues(context) {
        if (context === 'days') {
            displayValue('#js-days', window.days);
            displayValue('#js-hours', window.hours);
            displayValue('#js-minutes', window.minutes);
            displayValue('#js-seconds', window.seconds);
        } else if (context === 'hours') {
            displayValue('#js-hours', window.hours);
            displayValue('#js-minutes', window.minutes);
            displayValue('#js-seconds', window.seconds);
        } else if (context === 'minutes') {
            displayValue('#js-minutes', window.minutes);
            displayValue('#js-seconds', window.seconds);
        }
    }

    function displayValue(target, value) {
        var newDigit = $('<span></span>');
        $(newDigit).text(pad(value))
            .addClass('new');
        $(target).prepend(newDigit);
        $(target).find('.current').addClass('old').removeClass('current');
        setTimeout(function () {
            $(target).find('.old').remove();
            $(target).find('.new').addClass('current').removeClass('new');
        }, 900);
    }

    function pad(number) {
        return ("0" + number).slice(-2);
    }


    /* ================================================================================================== */
    /* CALENDER TIME ZONE */
    /* ================================================================================================== */

    function cal_time_zone (){
        var cal_zone = jQuery('.cal-time-zone > div');
        //console.log(cal_zone);

        var myFrame = jQuery(".ipWidget-Calendar iframe").contents().find('body');
         var cal_item = myFrame.find('.item-wrap');
         //console.log(cal_item);

         for (let i = 0; i < cal_zone.length; i++) {
            for (let j = 0; j < cal_item.length; j++) {
                cal_item[i].append(cal_zone[i]);
            }
        }
    };

    const myInterval = setInterval(cal_time_zone, 1000);
    setTimeout(function() {
        clearInterval(myInterval); console.log('DONE');
    }, 30000);


}(jQuery));