(function ($) {

    //DOM ready body class
    var DOMready = (function () {
        $(function () {
            $('.counter').counterUp({
                delay: 10,
                time: 1000
            });
        });
    }());

    /* ================================================================================================== */
    /* Smooth Scrolling Module */
    /* ================================================================================================== */

    // http://css-tricks.com/snippets/jquery/smooth-scrolling/
    var smoothScrollingModule = (function () {
        $(function () {
            $('#navigation-bar nav a[href*=#]:not([href=#])').click(function () {
                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                        $('html,body').animate({
                            scrollTop: target.offset().top - 100
                        }, 1000);
                        return false;
                    }
                }
            });
        });
    }());

    /* ================================================================================================== */
    /* Nav Sticky Design */
    /* ================================================================================================== */
    var navDesign = (function () {
        $(function () {
            var yourNavigation = $("#navigation-bar");
            stickyDiv = "sticky";
            yourHeader = $('#hero').height();

            stickyHeight = yourHeader - 65;

            $(window).scroll(function () {

                if ($(this).scrollTop() > stickyHeight) {
                    yourNavigation.addClass(stickyDiv);
                } else {
                    yourNavigation.removeClass(stickyDiv);
                }
            });
        });
    }());

}(jQuery));