(function() {
	"use strict";

	/* ================================================================================================== */
	/* Inject Header Module */
	/* ================================================================================================== */


	(function() {
		var link = document.querySelector("link[rel*='icon']") || document.createElement('link');
		link.type = 'image/png';
		link.rel = 'shortcut icon';
		link.href = 'https://ps-eu-eventscloud-com.s3.amazonaws.com/_internal-projects/aventri-survey-2020/favicon.png';
		document.getElementsByTagName('head')[0].appendChild(link);
	})();

	var injectHeader = (function() {
		document.addEventListener("DOMContentLoaded", function() {
			var header =document.createElement("header"),
			classes = header.classList, 
			target = document.querySelector("body");

			header.innerHTML = '<div class="inner-wrap"><div class="event-logo"><img src="https://ps-eu-eventscloud-com.s3.amazonaws.com/_internal-projects/aventri-survey-2020/images/logo.png" alt="Aventri"></div></div>';

			if(target !== null){
				target.insertBefore(header, target.firstChild);
			}
		});

	}()); 

	var heroBanner = (function() {
		document.addEventListener("DOMContentLoaded", function() {
			var hero = document.querySelector(".logo-img-container");
			var heroImg = hero.querySelector("img").getAttribute('src');
      hero.style.backgroundImage = "url('"+heroImg+"')";
      hero.classList.add("custom");
    });
	}()); 


	/* ================================================================================================== */
	/* Modify Buttons Module */
	/* ================================================================================================== */

	var modifyButtons = (function() {
		document.addEventListener("DOMContentLoaded", function() {
			var doneBtn = document.querySelector('#done_butt.r-button');
			var prevBtn = document.querySelector('#prev_butt.r-button');

			if(doneBtn !== null) {
				doneBtn.addEventListener("click", function(event){
					event.preventDefault();
					document.querySelector('#done_butt.r-desktop').click();
				});
			}

			if(prevBtn !== null) {
				prevBtn.addEventListener("click", function(event){
					event.preventDefault();
					document.querySelector('#prev_butt.r-desktop').click();
				});
			}
		});
	}());

	/* ================================================================================================== */
	/* Add Meta Data Module */
	/* ================================================================================================== */

	var addMetaData = (function() {
		document.addEventListener("DOMContentLoaded", function() {
			var viewport = document.createElement('meta');
			viewport.name="viewport";
			viewport.content = "width=device-width, initial-scale=1";
			document.getElementsByTagName('head')[0].appendChild(viewport);
		});
	}());



	(function ($) {

		/* ================================================================================================== */
		/* body */
		/* ================================================================================================== */
		var body = (function () {
			$(function () {
				$("body").addClass("custom-form");
				$("td.red").closest("div").addClass("has-error");

			});
		}());

		/* ================================================================================================== */
		/* Hero */
		/* ================================================================================================== */
		var heroBanner = (function () {
			$(function () {
				$(".logo-img-container").insertAfter("header");

			});
		}());

		/* ================================================================================================== */
		/* radio buttons to badges */
		/* ================================================================================================== */
		var radioBadges = (function () {
			$(function () {
				var questionContainer;
				var page = $("input[name=pagekey]").val();
				$('form').find('> div').each(function(index, elem) {
					var thisLabel = $(this).find('table > tbody > tr >td').text();
					if ( $("input[name=pagekey]").val() === "0") {
						$(this).attr('data-label', thisLabel);
					}
					$(this).attr('data-page', page);
					$(this).attr('data-question',index);
				});

				$('div[id]').each(function(i, elem) {
					questionContainer = $(this);
          // Checkboxes
          $('input[type="checkbox"]').each(function() {
          	$(this).prependTo( $(this).parent('td').next('td'));
          });
        });

      // Radio Buttons
      $('input[type="radio"]').each(function(index, element) {
      	var label = $(this).parent('td').siblings().find('label');
      	$(this).closest('table').parent('td').addClass('label-container').prepend( label );
      	$(this).closest('table').parent('td').prepend( $(this) );
      });

      $('.label-container').parent('tr').addClass('label-row');

      $('.label-row').each(function(index, element) {
      	var rowChildren = $(this).find('> td').length;
      	$(this).attr('data-children', rowChildren);
      });

    });
		}());

	}(jQuery));
}());

