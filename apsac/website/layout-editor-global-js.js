function loadScript(src) {
    let script = document.createElement('script');
    script.src = src;
    script.async = true;
    document.getElementsByTagName('body')[0].appendChild(script);
}
loadScript("https://ps.eventscloud.com/_internal-projects/website-designs/dubai-design/js/waypoints.min.js");
loadScript("https://ps.eventscloud.com/_internal-projects/website-designs/dubai-design/js/counter.js");
loadScript("https://ps.eventscloud.com/_internal-projects/website-designs/dubai-design-v2/js/custom-website.js");