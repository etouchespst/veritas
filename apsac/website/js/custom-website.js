(function($) {

    /* ================================================================================================== */
    /* Smooth Scrolling Module */
    /* ================================================================================================== */

    // http://css-tricks.com/snippets/jquery/smooth-scrolling/
    var smoothScrollingModule = (function() {
        $(function() {
            $('.navbar-nav li a[href*=#]').click(function() {
                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                        $('html,body').animate({
                            scrollTop: target.offset().top - 100
                        }, 1000);
                        return false;
                    }
                }
            });
        });
    }());

    /* ================================================================================================== */
    /* Navbar Hash Fix */
    /* ================================================================================================== */

    /* var hashFix = (function() {
        $(function() {

            $('.navbar-nav li a').each(function() {
                var hash = $(this).prop('hash'),
                    url = $(this).attr('href');
                    console.log('hash' + hash);

                if (url.indexOf('#') > -1) {
                    $(this).attr('href', hash);
                }

            });
        });
    }());
    */


    /* ================================================================================================== */
    /* Section Headers */
    /* ================================================================================================== */
    var sectionHeader = (function() {
        $(function() {
            $('.section-heading h4').wrapInner('<span></span>');
        });
    }());

    /* ================================================================================================== */
    /* DOM ready body class */
    /* ================================================================================================== */

    var DOMready = (function() {
        $(function() {

            var counter = $('#counter-section .ipWidget-CKEditor:first-child > div');

            $(counter).counterUp({
                delay: 10,
                time: 1000
            });
        });
    }());

    /* ================================================================================================== */
/* Accordion Module */
/* ================================================================================================== */

var accordions = (function(){
    $(function(){
        $('.accordion-section').parents('.ipWidget-Container').addClass('accordion-wrap');
        $('.accordion-section').on('click', '.column .ipBlock > .ipWidget:first-child', function(e){
            e.preventDefault();
            var $this = $(this),
                accordion = $this.parents('.accordion-section'),
                items = accordion.find('.column'),
                item = $this.parents('.column');
            
            // If already open then close accordion
            if(item.hasClass('selected')){
                item
                    .removeClass('selected')
                    .find('.ipBlock >.ipWidget:first-child ~ .ipWidget')
                    .slideUp(300, function(){
                        item.removeClass('active');
                    });
            // Open accordion
            } else {
                items
                    .removeClass('selected')
                    .find('.ipBlock >.ipWidget:first-child ~ .ipWidget')
                    .slideUp(300, function(){
                        items.removeClass('active');
                    });

                item
                    .addClass('selected')
                    .find('.ipBlock >.ipWidget:first-child ~ .ipWidget')
                    .slideDown(300, function(){
                        item.addClass('active');
                    });
            }  
        });
    });
}());


    /* ================================================================================================== */
/* AGENDA POP_UP */
/* ================================================================================================== */


setTimeout( function addId() {
var head = document.getElementsByClassName("etouches-agenda-popup");
for (var j=0; j<head.length; j++)
{
 document.getElementsByClassName("etouches-agenda-popup")[j].setAttribute("class","cus-agenda-popup-"+j);
}
},3000);






}(jQuery));