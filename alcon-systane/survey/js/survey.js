
jQuery(document).ready(function ($) {
    //jQuery('<meta name="viewport" content="width=device-width, initial-scale=1.0">').appendTo("head");
    //jQuery('body').addClass('dom-ready esurvey');
    $('script[src="/include/scripts/prototype.js"]').remove();
    $('<meta name="viewport" content="width=device-width, initial-scale=1.0">').appendTo("head");
});

(function ($) {

    //DOM ready body class
    var DOMready = (function () {
        $(function () {
            $('body').addClass('dom-ready esurvey');
            if (HELP.isiPhone) {
                $('body').addClass('iphone');

            }
        });
    }());

    /* ================================================================================================== */
    /* HELP Module */
    /* ================================================================================================== */

    var HELP = (function () {

        var pub = {},
            ua = navigator.userAgent,
            msie = ua.indexOf("MSIE ");

        pub.isTouchDevice = ('ontouchstart' in document.documentElement);
        pub.isiPad = (ua.match(/iPad/i) !== null);
        pub.isiPhone = (navigator.platform.indexOf('iPhone') !== -1) || (navigator.platform.indexOf('iPod') !== -1);
        pub.isAndroid = (ua.indexOf('Android') !== -1);
        pub.isAndroidVersion = ((pub.isAndroid) ? parseFloat(ua.match(/Android\s+([\d\.]+)/)[1]) : false);
        pub.isIE = (ua.indexOf('MSIE ') !== -1 || ua.match(/Trident/)); //Trident is for IE11
        pub.ieVersion = (parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)))) || false;

        return pub

    }());
    
    /* ================================================================================================== */
    /* responsive layout */
    /* ================================================================================================== */
    var resLayout = (function () {
        $(function () {
            $('.mobile-responsive').parent().parent().addClass("res-one");
            $('.res-one').next().addClass("res-two");
        });
    }());

}(jQuery));