(function ($) {

    /* ================================================================================================== */
    /* Smooth Scrolling Module */
    /* ================================================================================================== */

    // http://css-tricks.com/snippets/jquery/smooth-scrolling/
    var smoothScrollingModule = (function () {
        $(function () {
            $('.navbar-nav li a[href*=#]').click(function () {
                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                        $('html,body').animate({
                            scrollTop: target.offset().top - 100
                        }, 1000);
                        return false;
                    }
                }
            });
        });
    }());

    /* ================================================================================================== */
    /* Navbar Hash Fix */
    /* ================================================================================================== */

    /* var hashFix = (function() {
        $(function() {

            $('.navbar-nav li a').each(function() {
                var hash = $(this).prop('hash'),
                    url = $(this).attr('href');
                    console.log('hash' + hash);

                if (url.indexOf('#') > -1) {
                    $(this).attr('href', hash);
                }

            });
        });
    }());
    */


    /* ================================================================================================== */
    /* Section Headers */
    /* ================================================================================================== */
    var sectionHeader = (function () {
        $(function () {
            $('.section-heading h4').wrapInner('<span></span>');
        });
    }());

    /* ================================================================================================== */
    /* DOM ready body class */
    /* ================================================================================================== */

    // var DOMready = (function () {
    //     $(function () {

    //         var counter = $('#counter-section .ipWidget-CKEditor:first-child > div');

    //         $(counter).counterUp({
    //             delay: 10,
    //             time: 1000
    //         });
    //     });
    // }());

    /* ================================================================================================== */
    /* Accordion Module */
    /* ================================================================================================== */

    var accordions = (function () {
        $(function () {
            console.log('Hello!!');
            $('.accordion-section').parents('.ipWidget-Container').addClass('accordion-wrap');
            $('.accordion-section').on('click', '.column .ipBlock > .ipWidget:first-child', function (e) {
                e.preventDefault();
                var $this = $(this),
                    accordion = $this.parents('.accordion-section'),
                    items = accordion.find('.column'),
                    item = $this.parents('.column');

                // If already open then close accordion
                if (item.hasClass('selected')) {
                    item
                        .removeClass('selected')
                        .find('.ipBlock >.ipWidget:first-child ~ .ipWidget')
                        .slideUp(300, function () {
                            item.removeClass('active');
                        });
                    // Open accordion
                } else {
                    items
                        .removeClass('selected')
                        .find('.ipBlock >.ipWidget:first-child ~ .ipWidget')
                        .slideUp(300, function () {
                            items.removeClass('active');
                        });

                    item
                        .addClass('selected')
                        .find('.ipBlock >.ipWidget:first-child ~ .ipWidget')
                        .slideDown(300, function () {
                            item.addClass('active');
                        });
                }
            });
            //console.log("FAQ Loaded");
        });
    }());


    /* ================================================================================================== */
    /* AGENDA POP_UP */
    /* ================================================================================================== */


    setTimeout(function addId() {
        var head = document.getElementsByClassName("etouches-agenda-popup");
        for (var j = 0; j < head.length; j++) {
            document.getElementsByClassName("etouches-agenda-popup")[j].setAttribute("class", "cus-agenda-popup-" + j);
        }
    }, 3000);

    /* ================================================================================================== */
    /* Speakers Customizations */
    /* ================================================================================================== */

    var speakersCustomizations = (function () {
        $(function () {
            if ($('body.scholars').length > 0) {

                $("#custom-speakers-container .speaker").sort(function (a, b) {
                    var aText = $(a).attr('data-lname'),
                        bText = $(b).attr('data-lname');
                    aSort = $(a).attr('data-sort'),
                        bSort = $(b).attr('data-sort');
                    return aSort < bSort ? -1 : aSort > bSort ? 1 : 0 || aText < bText ? -1 : aText > bText ? 1 : 0;
                }).appendTo('#custom-speakers-container');
            }
        });
    }());

    /* ================================================================================================== */
    /* Countdown Timer */
    /* ================================================================================================== */

    var countdownTimer = (function () {
        $(function () {

            var timer = null;

            var eventStart = $('#eventdate').val();

            function makeTimer() {

                var endTime = new Date(eventStart),
                    endTime = (Date.parse(endTime)) / 1000,
                    now = new Date(),
                    now = (Date.parse(now) / 1000),
                    timeLeft = endTime - now;

                if (timeLeft > 0) {
                    var days = Math.floor(timeLeft / 86400),
                        hours = Math.floor((timeLeft - (days * 86400)) / 3600),
                        minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600)) / 60),
                        seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));
                    if (hours < "10") {
                        hours = "0" + hours;
                    }
                    if (minutes < "10") {
                        minutes = "0" + minutes;
                    }
                    if (seconds < "10") {
                        seconds = "0" + seconds;
                    }

                    $("#days .val").html(days);
                    $("#hours .val").html(hours);
                    $("#minutes .val").html(minutes);
                    $("#seconds .val").html(seconds);

                } else {
                    clearInterval(timer);
                    $("#days .val,#hours .val,#minutes .val,#seconds .val").html("0");
                }
            }
            timer = setInterval(function () {
                makeTimer();
            }, 1000);

        });
    }());

    /* ================================================================================================== */
    /* Scholar Filter */
    /* ================================================================================================== */

    var ScholarFilter = (function () {

        /*$('ul.nav.navbar-nav.navbar-center').each(function () {
            var select = $(document.createElement('select')).insertBefore($(this).hide());
            $('>li a', this).each(function () {
                var a = $(this).click(function () {
                        if ($(this).attr('target') === '_blank') {
                            window.open(this.href);
                        } else {
                            window.location.href = this.href;
                        }
                    }),
                    option = $(document.createElement('option')).appendTo(select).val(this.href).html($(this).html()).click(function () {
                        a.click();
                    });
                $('body #filer-section .row .column').append(select);
                $('nav.menu-widget.navbar.navbar-type-sticky.center-nav').remove();
            });
        });
        $('body #filer-section .row .column select').on('change', function () {
            var backtoTop = "<a href='#;' class='fa fa-arrow-circle-up backtoTop' style='float: right;'> Back to top</a>";
            $('.ipWidget .ipWidget-CKEditor h3').before(backtoTop);
            $('a.backtoTop').on('click', function () {
                $('a.backtoTop').remove();
                $("html, body").animate({
                    scrollTop: 0
                }, 1000);

            });
        });*/


        /*Create Dropdown*/
        var select = $('<select id="dropDownFilterId" class="dropDownFilter"><option selected="true" disabled="disabled">Select Country</option></select>').insertAfter('#filer-section nav.menu-widget');

        $('ul.nav.navbar-nav.navbar-center li').each(function () {
            var countryName = $.trim($(this).find('a').text());
            $('select.dropDownFilter').append('<option value="' + $(this).attr('class') + '">' + countryName + '</option>');
        });

        /*Scroll functionality*/
        $('select.dropDownFilter').on('change', function () {
            $('html, body').animate({
                scrollTop: $('#' + $(this).val()).offset().top - 200
            }, 2000);

            $("#" + $(this).val()).find('h3').append('<span id="backToFilter">Back to filter</span>');
        });

        /*Back to filter*/
        $("body").on('click', '#backToFilter', function (e) {
            e.preventDefault();
            $('html, body').animate({
                scrollTop: $("#dropDownFilterId").offset().top
            }, 2000);
            $('#backToFilter').remove();
        });

    }());

}(jQuery));