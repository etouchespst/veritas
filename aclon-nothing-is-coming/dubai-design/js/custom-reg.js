if(typeof jQuery !== 'undefined') {

	(function ($) {

		//safe way to use console.log
		$.log = function(logvar){
		    if ((window.console !== 'undefined')){
		        console.log(logvar);
		    }
		};

		//DOM ready body class
		var DOMready = (function(){
		  	$(function(){
		    	$('body').addClass('dom-ready ereg');
		    	// $( "style" ).remove();
		  	});
		}());

		/* ================================================================================================== */
		/* iphone number color issue */
		/* ================================================================================================== */

			$("head").append('<meta name="format-detection" content="telephone=no">');

		/* ================================================================================================== */
		/* Utils Module */
		/* ================================================================================================== */

		var utilsModule = (function () {

		    var getQueryVariable = function(variable){
		        var query = window.location.search.substring(1);
		        var vars = query.split("&");
		        for (var i=0;i<vars.length;i++) {
		            var pair = vars[i].split("=");
		            if(pair[0] == variable){return pair[1];}
		       }
		       return(false);
		    };

		    var checkForHash = function (hashCheck){
		    	if(window.location.hash === hashCheck) {
		    		return true;
		    	} 
		    };

		    return {
		        getQueryVariable : getQueryVariable,
		        checkForHash : checkForHash
		    };
		}());

		/* ================================================================================================== */
		/* HELP Module */
		/* ================================================================================================== */

		var HELP = (function () {

			var pub = {},
				ua = navigator.userAgent,
				msie = ua.indexOf("MSIE ");
				
			pub.isTouchDevice = ('ontouchstart' in document.documentElement);
			pub.isiPad = (ua.match(/iPad/i) !== null);
			pub.isiPhone = (navigator.platform.indexOf('iPhone') !== -1) || (navigator.platform.indexOf('iPod') !== -1);
			pub.isAndroid = (ua.indexOf('Android') !== -1);
			pub.isAndroidVersion = ((pub.isAndroid) ? parseFloat(ua.match(/Android\s+([\d\.]+)/)[1]) : false);
			pub.isIE = (ua.indexOf('MSIE ') !== -1 || ua.match(/Trident/));//Trident is for IE11
			pub.ieVersion = (parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)))) || false;

			return pub

		}());

		/* ================================================================================================== */
		/* Amend Header HTML Structure */
		/* ================================================================================================== */

		// var amendHeader = (function () {
		//     $(function () {
		//         $('.language-selector-container select').wrap('<div class="language-wrapper"><span></span></div>'); 
		//     }); 
		// }()); 


		/* ================================================================================================== */
		/* Smooth Scrolling Module */ 
		/* ================================================================================================== */

		// http://css-tricks.com/snippets/jquery/smooth-scrolling/
		// var smoothScrollingModule = (function () {
		// 	$(function() {
		// 	  	// $('a[href*=#]:not([href=#])').click(function() {
		// 	  	$('a.scroll').click(function() {
		// 	  	// $('body').on('click', 'a[href*=#]:not([href=#])', function() {
		// 	    	if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		// 	      		var target = $(this.hash);
		// 	      		target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		// 	      		if (target.length) {
		// 	        		$('html,body').animate({
		// 	          			scrollTop: target.offset().top - 80
		// 	        		}, 1000);
		// 	        		return false;
		// 	      		}
		// 	    	}
		// 	  	});
		// 	});
		// }());

		/* ================================================================================================== */
		/* View Content */
		/* ================================================================================================== */

		var viewContent = (function () {
		    $(function () {    	
	    		$('a.scroll').on('click', function(e){
	    			e.preventDefault();
					$('html,body').animate({
			  			scrollTop: $('.lobby-container').offset().top - 80
					}, 1000);
	    		});
		    }); 
		}()); 

		/* ================================================================================================== */
		/* VE Mobile Account */
		/* ================================================================================================== */

		var veMobileAccount = (function () {
		    $(function () {    	
	    		$('.user-profile-trigger').on('click', function(){
					$('html,body').animate({
			  			scrollTop: $('.profile').offset().top - 80
					}, 1000);
	    		});
		    }); 
		}()); 

		/* ================================================================================================== */
		/* Additional JS */
		/* ================================================================================================== */

		
		function checkboxClass() {
			$('input[type="checkbox"]').parents('.questionContainer').addClass('checkbox-wrap');
			$('#inner_content').append($(".customfooter"));
		}; 
		setTimeout(checkboxClass, 1000);
	


		
			


}(jQuery));
}